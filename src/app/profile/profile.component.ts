import {Component, OnInit} from '@angular/core';
import {User} from '../_models';
import {Router} from '@angular/router';
import {AlertService, AuthenticationService, UserService} from '../_services';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {first} from 'rxjs/operators';
import {environment} from '../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {UploadComponent} from '../upload/upload.component';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  currentUser: User;
  profileForm: FormGroup;
  loading = false;
  submitted = false;

  public imagePath;
  imgURL: any;
  selectedFile: File;

  constructor(
    private router: Router,
    private authenticationService: AuthenticationService,
    private formBuilder: FormBuilder,
    private userService: UserService,
    private alertService: AlertService,
    private http: HttpClient
  ) {
    this.authenticationService.currentUser.subscribe(x => this.currentUser = x);
  }

  ngOnInit() {
    this.profileForm = this.formBuilder.group({
      status: [''],
      lastName: ['', Validators.required],
      firstName: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      linkedin: [''],
      twitter: [''],
      website: [''],
      organisation: [''],
      bio: [''],
      profileImageUrl: ['']

      //password: ['', [Validators.required, Validators. minLength(6)]]
    });

    this.profileForm.patchValue({
      firstName: this.currentUser.firstName,
      lastName: this.currentUser.lastName,
      email: this.currentUser.email,
      status: this.currentUser.status,
      linkedin: this.currentUser.linkedin,
      twitter: this.currentUser.twitter,
      website: this.currentUser.website,
      organisation: this.currentUser.organisation,
      bio: this.currentUser.bio,
      _id: this.currentUser.id,
      profileImageUrl: this.currentUser.profileImageUrl === undefined ? 'https://secure.gravatar.com/avatar/71a311b16fb1a66d3cb621bfb95bfd4b?s=320&d=identicon' : this.currentUser.profileImageUrl
    });

  }

  // convenience getter for easy access to form fields
  get f() {
    return this.profileForm.controls;
  }

  onSubmitEdit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.profileForm.invalid) {
      return;
    }

    this.loading = true;

    console.log('VALUED NAMES ', this.profileForm.value);

    this.userService.update(this.currentUser._id, this.profileForm.value)
      .pipe(first())
      .subscribe(
        data => {
          this.alertService.success('Info profile edited successful', true);
          this.router.navigate(['/profile']);

          // update the current user
          this.currentUser.lastName = this.profileForm.value.lastName;
          this.currentUser.firstName = this.profileForm.value.firstName;
          this.currentUser.bio = this.profileForm.value.bio;
          this.currentUser.organisation = this.profileForm.value.organisation;
          this.currentUser.website = this.profileForm.value.website;
          this.currentUser.twitter = this.profileForm.value.twitter;
          this.currentUser.linkedin = this.profileForm.value.linkedin;
          this.currentUser.status = this.profileForm.value.status;
          this.currentUser.email = this.profileForm.value.email;

          this.currentUser.profileImageUrl = this.profileForm.value.profileImageUrl;

          // need to update storage :
          console.log('Setting storage to this : ' + JSON.stringify(this.currentUser));
          localStorage.setItem('currentUser', JSON.stringify(this.currentUser));

          // upload image
          // this.uploadFile();

        },
        error => {
          this.alertService.error(error);
          this.loading = false;
        });
  }

  onImageChanged(event) {
    this.selectedFile = event.target.files[0];
    console.log('FILE : ' + this.selectedFile.size);
  }

  preview(files) {
    if (files.length === 0) {
      return;
    }

    if (files[0].size > 2000000) {
      return;
    }

    const mimeType = files[0].type;
    if (mimeType.match(/image\/*/) == null) {
      return;
    }

    this.selectedFile = files[0];

    const reader = new FileReader();
    this.imagePath = files;

    reader.readAsDataURL(files[0]);
    reader.onload = (_event) => {
      this.imgURL = reader.result;
      console.log('resultttttttttttt : ' + reader.result);
      /*
            this.http.post(environment.uploads, reader.result, {
              reportProgress: true,
              observe: 'events'
            })
              .subscribe(event => {
                console.log(event); // handle event here
              });
       */
    };
  }


  uploadFile() {
    // this.http is the injected HttpClient
    console.log('UPLOADING STUFF ..');

    const uploadData = new FormData();
    console.log('UPLOADING STUFF ..' + JSON.stringify(uploadData));
    uploadData.append('profile-', this.imgURL);

    console.log(environment.uploads);

    this.http.post(environment.uploads, uploadData, {
      reportProgress: true,
      observe: 'events'
    })
      .subscribe(event => {
        console.log(event); // handle event here
      });
  }
}
