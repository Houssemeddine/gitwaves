import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {AlertService, AuthenticationService} from '@app/_services';
import {first} from 'rxjs/operators';
import {MemberService} from '@app/_services/member.service';
import {Member} from '@app/_models/member';
import {Project} from '@app/_models/project';
import {ProjectService} from '@app/_services/project.service';
import {User} from '@app/_models';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-membres',
  templateUrl: './membres.component.html',
  styleUrls: ['./membres.component.css']
})
export class MembresComponent implements OnInit {

  NewMemberForm: FormGroup;
  selectMembers: Member;
  memberslist: Member[] = [];
  currentProject: Project;
  currentUser: User;
  roles: string[] = ['Member', 'Maintainer'];
  role: string;
  currentUserSubscription: Subscription;

  constructor(private formBuilder: FormBuilder,
              private membreService: MemberService,
              private router: Router,
              private alertService: AlertService,
              private projectService: ProjectService,
              private authservice: AuthenticationService
  ) {
    this.currentUserSubscription = this.authservice.currentUser.subscribe(user => {
      this.currentUser = user;
    });
    if (this.projectService.currentProjectValue) {
      this.currentProject = this.projectService.currentProjectValue;
      console.log('we have a user ' + this.currentProject._id);
    } else {
      console.log('we DONT have a user');
    }

  }

  loading = false;
  membres: Member [];

  ngOnInit() {
    this.loadAllMembres();
    this.initForm();
    this.getAllMembers();
  }

  initForm() {
    this.NewMemberForm = this.formBuilder.group({
      name: ['', [Validators.required]],
    });

  }

  onSubmit() {
    console.log(this.role);
    //this.NewMemberForm.value.role=this.role;
    console.log('valeuur for role : ' + this.NewMemberForm.value.role);
    if (this.selectMembers) {
        this.membreService.new(this.NewMemberForm.value, this.currentProject._id, this.selectMembers._id).pipe(first())
          .subscribe(
            data => {
              this.alertService.success('New Member added to your project', true);
              this.router.navigate(['/project']);
            },
            error => {
              this.alertService.error(error);
              this.loading = false;
            });
        // this.ngOnInit()
      window.location.reload();
      this.alertService.success('New Member added to your project ', true);



    }else{
      this.alertService.error('select Member for added to your project ', true);
    }
  }

  private loadAllMembres() {
    this.membreService.getAllmemberfromProject(this.currentProject.link_identifier).subscribe((data: Member[]) => {
      console.log(data);
      this.membres = data;
    });
  }

  deleteBusiness(idmembre) {
    this.membreService.delete(this.currentProject._id, idmembre).subscribe(res => {
      console.log('Deleted');
      this.ngOnInit();
    });

    window.location.reload();
    this.loadAllMembres();
  }

  onSelect(member: Member) {
    this.selectMembers = member;
    console.log('selected id : ' + this.selectMembers._id);
  }

  getAllMembers() {
    this.membreService.getAll().subscribe(data => {
      this.memberslist = data;
    });
  }

  onselectRole(role: string) {
    this.role = role;
    console.log('role' + role);
  }

  ontestUser(membre: Member) {
    if (membre._id + '' == this.currentUser._id) {
      return false;
    } else {
      return true;
    }
  }
}
