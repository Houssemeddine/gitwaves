import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '@environments/environment';
import {Project} from '@app/_models/project';
import {BehaviorSubject, Observable} from 'rxjs';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})


export class ProjectService {
  private currentProjectSubject: BehaviorSubject<Project>;
  private currentProject: Observable<Project>;


  constructor(private http: HttpClient) {
    this.currentProjectSubject = new BehaviorSubject<Project>(JSON.parse(localStorage.getItem('currentProject')));
    this.currentProject = this.currentProjectSubject.asObservable();
  }

  public set currentProjectValue(project: Project) {
    //console.log("setttinggggggggggggggggg vaaaaaaaaaaaaaaaa"+JSON.stringify(project));
    this.currentProjectSubject.next(project);
    //this.currentProject.
  }

  public get currentProjectValue(): Project {
    return this.currentProjectSubject.value;
  }

  getAllProject(paginator) {
    return this.http.post<Project[]>(`${environment.apiUrl}/api/list/all/projects`, paginator);
  }

  /*
  Get all project for given User ID
   */
  getAllForGivenUser(id: string) {
    return this.http.get<Project[]>(`${environment.apiUrl}/api/list/projects/${id}`);
  }

  /*
 Get all project for given User NAME
  */
  getAllForGivenUserName(username: string) {
    return this.http.get<Project[]>(`${environment.apiUrl}/api/list/user/projects/${username}`);
  }

  /*
  Get Project by given ID
   */
  getById(id: number) {
    return this.http.get(`${environment.apiUrl}/api/projects/${id}`);
  }

  /*
  identifier : root/projectName  as a string
  */
  getProjectByIdentifier(identifier: string) {
    return this.http.get<Project>(`${environment.apiUrl}/api/project/${identifier}`)  .pipe(map(project => {
      // login successful if there's a jwt token in the response

        // store user details and jwt token in local storage to keep user logged in between page refreshes
        localStorage.setItem('currentProject', JSON.stringify(project));
        this.currentProjectSubject.next(project);


      return project;
    }));
  }

  new(project: Project) {
    return this.http.post(`${environment.apiUrl}/api/projects/new`, project);
  }


  update(project: Project) {
    return this.http.put(`${environment.apiUrl}/projects/${project.id}`, project);
  }

  delete(id: number) {
    return this.http.delete(`${environment.apiUrl}/projects/${id}`);
  }

  forkProjectWithUserIDAndProjectID(userId: number, projectId: number, project) {
    return this.http.post(`${environment.apiUrl}/api/fork/${projectId}/user/${userId}`, project);
  }

  startProject(id: number) {
    return this.http.get(`${environment.apiUrl}/api/star/project/${id}`);
  }
}
