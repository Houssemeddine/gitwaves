import { Injectable } from '@angular/core';
import {Branche} from '@app/_models/branche';
import {environment} from '@environments/environment';
import {HttpClient} from '@angular/common/http';
import {Issues} from '@app/_models/issues';

@Injectable({
  providedIn: 'root'
})
export class IssuesService {

  constructor(    private http: HttpClient,
  ) { }

  newIssues(issues: Issues, brancheid: number) {
    return this.http.post(`${environment.apiUrl}/api/new/issues/${brancheid}`, issues);
  }
  getIssues(idproject: number) {
    return this.http.get<Issues[]>(`${environment.apiUrl}/api/issues/${idproject}`);
  }
  getOneIssues(id: number) {
    return this.http.get<Issues>(`${environment.apiUrl}/api/issue/${id}`);
  }


}
