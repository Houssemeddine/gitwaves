import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {environment} from "@environments/environment";
import {Member} from "@app/_models/member";

@Injectable({
  providedIn: 'root'
})
export class MemberService {

  constructor(private http: HttpClient) { }

  getAll() {
    return this.http.get<Member[]>(`${environment.apiUrl}/api/membres`);
  }
  getAllmemberfromProject(linkidentif: string) {
    return this.http.get<Member[]>(`${environment.apiUrl}/api/membres/${linkidentif}`);
  }

  getById(id: number) {
    return this.http.get(`${environment.apiUrl}/membres/${id}`);
  }

  new(membre: Member , projectid: number , userid: number) {
    membre.user_id = userid;
    console.log("le membre :"+membre.user_id)
    return this.http.post(`${environment.apiUrl}/api/membres/new/${projectid}`, membre);
  }

  update(membre: Member) {
    return this.http.put(`${environment.apiUrl}/membres/${membre._id}`, membre);
  }

  delete(iduser : number , idproject : number) {
    return this.http.delete(`${environment.apiUrl}/api/membres/delete/${iduser}/${idproject}`);
  }
}
