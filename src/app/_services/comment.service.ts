import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '@environments/environment';
import {Comments} from '@app/_models/Comment';

@Injectable({
  providedIn: 'root'
})
export class CommentService {

  constructor(private http: HttpClient) { }
  newComment(comment: Comments , issueid: number, idcreator: string) {
    comment.issues_id = issueid;
    comment.creator_id = idcreator;
    return this.http.post(`${environment.apiUrl}/api/new/comment/${issueid}`, comment);
  }
  getCommentforISsues( issueid: number) {
    return this.http.get<Comments[]>(`${environment.apiUrl}/api/comment/${issueid}`);
  }



}
