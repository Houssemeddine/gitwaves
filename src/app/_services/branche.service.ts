import {Injectable} from '@angular/core';
import {Branche} from '@app/_models/branche';
import {environment} from '@environments/environment';
import {HttpClient} from '@angular/common/http';
import {Project} from '@app/_models/project';
import {Content} from '@app/_models/Content';

@Injectable({
  providedIn: 'root'
})
export class BrancheService {

  constructor(
    private http: HttpClient,
  ) {
  }

  loadBrancheContent(link_identifier, branchName) {
    return this.http.get<Content>(`${environment.apiUrl}/api/get/content/${link_identifier}/${branchName}`);
  }

  loadCommitContent(link_identifier, branchName, commitNumber) {
    return this.http.get<Content>(`${environment.apiUrl}/api/get/content/${link_identifier}/${branchName}/${commitNumber}`);
  }

  loadBranche(link_identifier) {
    return this.http.get<Branche[]>(`${environment.apiUrl}/api/list/branches/${link_identifier}`);
  }

  newBranche(branche: Branche, projectid: number, brancheid: number) {
    branche.clone_id = brancheid;
    return this.http.post(`${environment.apiUrl}/api/new/branch/${projectid}/from/${brancheid}`, branche);
  }
  getBranchebyId(brancheid : number ){
    return this.http.get<Branche>(`${environment.apiUrl}/api/branch/${brancheid}`);

  }
}
