import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Issues} from '@app/_models/issues';
import {environment} from '@environments/environment';
import {Commit} from '@app/_models/Commit';

@Injectable({
  providedIn: 'root'
})
export class CommitService {

  constructor( private http: HttpClient) { }

  getCommits(id: number) {
    return this.http.get<Commit[]>(`${environment.apiUrl}/api/commits/${id}`);
  }
}
