import {Branche} from '@app/_models/branche';

export class Issues {
   _id: number;
   name: string;
   description: string;
  created_at: Date;
   last_update: Date;
   stars: number;
   visibility: string;
   link_identifier: string;
   creator_id: string;
   project_id: number;
   branche_id: number;
   forked: boolean;
   issued: Branche[];


}
