import {User} from '@app/_models/user';
import {Branche} from '@app/_models/branche';
import {Content} from '@app/_models/Content';

export class Project {
  id: number;
  _id: number;

  name: string;
  description: string;
  etat: number;
  date: Date;
  last_update: Date;
  stars: number;
  visibility: string;
  current_user_role: string;
  link_identifier: string;
  creator_id: User;
  forked: boolean;
  branched: Branche[];
  forkes: number;
}
