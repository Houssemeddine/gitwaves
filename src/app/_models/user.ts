﻿export class User {
  id: number;
  username: string;
  password: string;
  firstName: string;
  lastName: string;
  profileImageUrl: string;
  token: string;
  linkedin: string;
  twitter: string;
  website: string;
  organisation: string;
  bio: string;
  status: string;
  _id: string;
  email: string;
}
