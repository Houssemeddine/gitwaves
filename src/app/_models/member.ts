export class Member {
  _id: number;
  name: string;
  role: string;
  username: string;
  user_id: number;
}
