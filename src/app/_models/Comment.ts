
export class Comments {
  id: number;
  _id: number;
  description: string;
  date: Date;
  last_update: Date;
  stars: number;
  visibility: string;
  link_identifier: string;
  creator_id: string;
  issues_id: number;
  created_at: Date;

}
