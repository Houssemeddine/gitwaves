import {User} from '@app/_models/user';

export class Commit {
  _id: number;
  name: string;
  hash_value: string;
  description: string;
  created_at: Date;
  timestamp: string;
  //creator_id: string;
  creator_id: User;

}
