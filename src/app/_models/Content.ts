export class Content {
  text: string;
  children: Content[];
}
