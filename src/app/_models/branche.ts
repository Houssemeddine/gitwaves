import {Content} from '@app/_models/Content';

export class Branche {
  id: number;
  _id: number;
  name: string;
  last_update: Date;
  hash_value: string;
  created_at: Date;
  content: Content;
  clone_id: number;
}
