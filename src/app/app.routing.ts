﻿﻿import {HelpComponent} from '@app/help/help.component';
﻿import {Routes, RouterModule} from '@angular/router';
import {HomeComponent} from './home';
import {LoginComponent} from './login';
import {RegisterComponent} from './register';
import {AuthGuard} from './_guards';
import {ProjectComponent} from '@app/project/project.component';
import {NewProjectComponent} from './project/new-project/new-project.component';
import {ProfileComponent} from '@app/profile/profile.component';
import {GraphComponent} from '@app/project/graph/graph.component';
import {MembresComponent} from '@app/membres/membres.component';
import {BranchComponent} from '@app/project/branch/branch.component';
import {NgModule} from '@angular/core';
import {NewBranchComponent} from '@app/project/branch/new-branch/new-branch.component';
import {ProjectsComponent} from '@app/project/projects/projects.component';
import { GetBranchFromProjectComponent } from './project/branch/get-branch-from-project/get-branch-from-project.component';
import { IssuesComponent } from './project/issues/issues.component';
import { NewIssuesComponent } from './project/issues/new-issues/new-issues.component'
import { GetIssuesComponent } from './project/issues/get-issues/get-issues.component'
import {BrancheDataComponent} from '@app/project/branch/branche-data/branche-data.component';

const appRoutes: Routes = [
  {path: '', component: HomeComponent, canActivate: [AuthGuard]},
  //{path: ':username', component: HomeComponent, canActivate: [AuthGuard]},

  {path: 'login', component: LoginComponent},
  {path: 'register', component: RegisterComponent},


  {path: 'explore', component: ProjectsComponent},

  {
    path: 'project',
    children: [
      {path: '', redirectTo: 'profile', pathMatch: 'full'},
      {path: ':username/:projectname', component: ProjectComponent},
      {path: ':username/:projectname/branches/new', component: NewBranchComponent},
      {path: ':username/:projectname/branches/tree/:branchename', component: BrancheDataComponent},
      {path: ':username/:projectname/branches', component: GetBranchFromProjectComponent},
      {path: ':username/:projectname/issues', component: IssuesComponent},
      {path: ':username/:projectname/issue/:id', component: GetIssuesComponent},
      {path: ':username/:projectname/issues/new', component: NewIssuesComponent},
      {path: ':username/:projectname/membres', component: MembresComponent},
      {path: ':username/:projectname/graph', component: GraphComponent},
      {path: 'new', component: NewProjectComponent},
    ]
  },


  {path: 'profile', component: ProfileComponent},
  {path: 'profile', component: ProfileComponent},
  {path: 'issues',  component: IssuesComponent},
  {path: 'branches', component: BranchComponent},

  {path: 'help', component: HelpComponent},



  // otherwise redirect to home
  {path: '**', redirectTo: ''}
];


export const routing = RouterModule.forRoot(appRoutes);
