﻿﻿import {Component, OnInit, OnDestroy} from '@angular/core';
import {Observable, Subscription} from 'rxjs';
import {first, map} from 'rxjs/operators';

import {User} from '@app/_models';
import {UserService, AuthenticationService} from '@app/_services';
import {ProjectService} from '@app/_services/project.service';
import {Project} from '@app/_models/project';
import {ActivatedRoute, Router} from '@angular/router';


@Component({templateUrl: 'home.component.html'})
export class HomeComponent implements OnInit, OnDestroy {
  currentUser: User;
  currentUserSubscription: Subscription;
  users: User[] = [];

  projectService: ProjectService;
  subscribedToProjects: Project[] = [];
  ownedProject: Project[] = [];

  loadedProjects: Project[] = [];

  constructor(
    private authenticationService: AuthenticationService,
    private userService: UserService,
    private  prjService: ProjectService,
    private route: ActivatedRoute,
    private router: Router,
  ) {
    this.currentUserSubscription = this.authenticationService.currentUser.subscribe(user => {
      this.currentUser = user;
      this.projectService = prjService;
    });
  }


  // todo : make tab workink in html [ personnal -> owndprojec , forked -> forkd projct ]

  ngOnInit() {
    console.log('Loading all project : ');

    if (this.route.snapshot.params['username'] != undefined) {
      this.projectService.getAllForGivenUserName(this.route.snapshot.params['username'])
        .pipe(first())
        .subscribe(projects => {
          console.log('Received those projects' + JSON.stringify(projects));

          // @ts-ignore
          this.ownedProject = Object.assign([], projects.member);

          this.ownedProject.forEach(x => {
            this.loadedProjects.push(x);
          });

          // @ts-ignore
          this.subscribedToProjects = Object.assign([], projects.forked);
          this.subscribedToProjects.forEach(x => {
            this.loadedProjects.push(x);
          });


        });
    } else {
      this.projectService.getAllForGivenUser(this.authenticationService.currentUserValue._id)
        .pipe(first()).subscribe(projects => {

        //this.subscribedToProjects = projects ['subscribed'];
        //this.ownedProject = projects ['member'];

        console.log(projects['forked']);

        var pr: any;
        for (pr in projects ['member']) {

          var project: Project = new Project();
          //  console.log(memberProjects[pr]); // THIS WILL RETURN RELATION AND IT's TARGET NODE
          project.id = projects['member'][pr]['node']['_id'];
          project.name = projects['member'][pr]['node']['name'];
          project.date = projects['member'][pr]['node']['date'];
          project.description = projects['member'][pr]['node']['description'];
          project.last_update = projects['member'][pr]['node']['last_update'];
          project.stars = projects['member'][pr]['node']['stars'];
          project.etat = projects['member'][pr]['node']['etat'];
          project.link_identifier = projects['member'][pr]['node']['link_identifier'];
          project.current_user_role = projects['member'][pr]['role'];

          this.ownedProject.push(project);
          this.loadedProjects.push(project);

        }

        for (pr in projects ['forked']) {

          var project: Project = new Project();
          //  console.log(memberProjects[pr]); // THIS WILL RETURN RELATION AND IT's TARGET NODE
          project.id = projects ['forked'][pr]['node']['_id'];
          project.name = projects ['forked'][pr]['node']['name'];
          project.date = projects ['forked'][pr]['node']['date'];
          project.description = projects ['forked'][pr]['node']['description'];
          project.last_update = projects ['forked'][pr]['node']['last_update'];
          project.stars = projects ['forked'][pr]['node']['stars'];
          project.etat = projects ['forked'][pr]['node']['etat'];
          //project.current_user_role = projects['forked'][pr]['role'];

          project.link_identifier = this.currentUser.username + '/' + projects['forked'][pr]['node']['name'];
          project.forked = true;

          //console.log(project);


          this.subscribedToProjects.push(project);
          this.loadedProjects.push(project);
        }

      });
    }
  }

  getProjects(): Promise<Project[]> {
    return Promise.resolve(this.loadedProjects);
  }

  getProject(id: number): Promise<Project> {
    return this.getProjects()
      .then(models => models.find(model => model.id === id));
  }

  ngOnDestroy() {
    // unsubscribe to ensure no memory leaks
    this.currentUserSubscription.unsubscribe();
  }

  deleteUser(id: number) {
    this.userService.delete(id).pipe(first()).subscribe(() => {
      this.loadAllUsers();
    });
  }

  private loadProjectProfile(link_identifier: string) {
    this.projectService.getProjectByIdentifier(link_identifier);
  }


  private loadAllUsers() {
    this.userService.getAll().pipe(first()).subscribe(users => {
      this.users = users;
    });
  }
}
