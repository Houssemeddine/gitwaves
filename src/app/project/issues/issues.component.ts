import {Component, OnInit} from '@angular/core';
import {User} from '@app/_models';
import {Project} from '@app/_models/project';
import {ActivatedRoute, Router} from '@angular/router';
import {AuthenticationService, UserService} from '@app/_services';
import {ProjectService} from '@app/_services/project.service';
import {Issues} from '@app/_models/issues';
import {IssuesService} from '@app/_services/issues.service';
import {Comments} from '@app/_models/Comment';
import {BrancheService} from '@app/_services/branche.service';
import {CommentService} from '@app/_services/comment.service';
import {environment} from '@environments/environment';

@Component({
  selector: 'app-issues',
  templateUrl: './issues.component.html',
  styleUrls: ['./issues.component.css']
})
export class IssuesComponent implements OnInit {
  currentUser: User;
  currentProject: Project;
  issuess: Issues[];
  private issueService: IssuesService;


  userProject: string;
  projectName: string;

  constructor(
    private router: Router,
    private authenticationService: AuthenticationService,
    private projectService: ProjectService,
    private issuesService: IssuesService,
    private  brancheService: BrancheService,
    private userService: UserService,
    private commentService: CommentService,
    private route: ActivatedRoute,
  ) {
    this.issueService = issuesService;

    if (this.authenticationService.currentUserValue) {
      this.currentUser = this.authenticationService.currentUserValue;
      console.log('we have a user ' + this.currentUser._id);
    } else {
      console.log('we DONT have a user');
    }
    if (this.projectService.currentProjectValue) {
      this.currentProject = this.projectService.currentProjectValue;
      console.log('we have a user ' + this.currentProject._id);
    } else {
      console.log('we DONT have a user');
    }

    this.userProject = this.route.snapshot.params['username'] ;
    this.projectName =  this.route.snapshot.params['projectname'];

  }

  ngOnInit() {
    this.loadIssues();
  }

  private loadIssues() {

    console.log(this.currentProject.name);
    console.log("/api/issues/"+this.currentProject._id);

    this.issueService.getIssues(this.currentProject._id).subscribe((data: Issues[]) => {

      this.issuess = data;
      // console.log(this.issuess);
      this.issuess.forEach(res => {
        console.log('id: ' + res._id);
      });

    });
  }

  private loadCreatorIssue(id: number) {

    this.userService.getById(id + '').subscribe(
      res => {
        console.log(res.username);
        return res.username;
      }
    );


  }

  private loadBranche(id: number) {

    this.brancheService.getBranchebyId(id).subscribe(
      res => {
        //console.log(res);
        return res.name;
      }
    );
  }

  // private loadComment(id:number) {
  //
  //       this.commentService.getCommentforISsues(id).subscribe(
  //         (res: Comments[]) => {
  //           return res.length ;
  //         }
  //       )
  //
  //
  //     }


}
