import {Component, Input, OnInit} from '@angular/core';
import {User} from '@app/_models';
import {Project} from '@app/_models/project';
import {ActivatedRoute, Router} from '@angular/router';
import {AlertService, AuthenticationService} from '@app/_services';
import {ProjectService} from '@app/_services/project.service';
import {Branche} from '@app/_models/branche';
import {BrancheService} from '@app/_services/branche.service';
import {first} from 'rxjs/operators';
import {Issues} from '@app/_models/issues';
import {IssuesService} from '@app/_services/issues.service';

@Component({
  selector: 'app-new-issues',
  templateUrl: './new-issues.component.html',
  styleUrls: ['./new-issues.component.css']
})
export class NewIssuesComponent implements OnInit {
  currentUser: User;
  currentProject: Project;

  branches: Branche[] = [];
  selectedBranche: Branche;

  link_identifier: string;
  project: Project = new Project();
  brancheList: Branche [] = [];
@Input() name: string;
@Input() description : string;

userProject: string;
projectName: string;


  constructor(
    private router: Router,
    private authenticationService: AuthenticationService,
    private projectService: ProjectService,
    private brancheService: BrancheService,
    private activatedRoute: ActivatedRoute,
    private alertService: AlertService,
    private issuesService: IssuesService
  ) {
    if (this.authenticationService.currentUserValue){
      this.currentUser  = this.authenticationService.currentUserValue;
      console.log("we have a user " + this.currentUser._id);
    }else{
      console.log("we DONT have a user");
    }

    this.userProject = this.activatedRoute.snapshot.params['username'] ;
    this.projectName =  this.activatedRoute.snapshot.params['projectname'];

  }


  createissues() {

  //  console.log('Branche  ' + this.branchName);
    if (this.name.length < 2) {
      this.alertService.error('Please insert a valid branch name', true);
    } else {
      let tmp: Issues = new Issues();
      tmp.name = this.name;
      tmp.description = this.description;
      tmp.creator_id = this.currentUser._id;
      tmp.project_id = this.currentProject._id;
      tmp.branche_id = this.selectedBranche._id;

      console.log("l'issues ajoute est :"+tmp.description);
      this.issuesService.newIssues(
        tmp,
        this.selectedBranche._id
      ).subscribe(
        data => {
          console.log('redirecting');
          this.alertService.success('Created issue successfully', true);
          this.router.navigate(['/project/'+this.userProject+'/'+this.currentProject.name+'/issues']);
        },
        error => {
          this.alertService.error(error);
        });
    }
  }


  ngOnInit() {
    this.link_identifier = this.activatedRoute.snapshot.params['username'] + '/' + this.activatedRoute.snapshot.params['projectname'];
    console.log('Project to load DATA : ' + this.link_identifier);

    this.projectService.getProjectByIdentifier(this.link_identifier).pipe(first()).subscribe(project => {

      // @ts-ignore
      this.project = Object.assign(new Project(), project.project);
      this.project.branched = Object.assign([], project.branched);
      this.selectedBranche = this.project.branched[0];
      this.currentProject = project;

    });
  }
  onSelect(branche: Branche) {
    this.selectedBranche = branche;
    console.log('selected id : ' + branche._id);
  }
}
