import { Component, OnInit } from '@angular/core';
import {User} from '@app/_models';
import {Project} from '@app/_models/project';
import {Issues} from '@app/_models/issues';
import {IssuesService} from '@app/_services/issues.service';
import {ActivatedRoute, Router} from '@angular/router';
import {AlertService, AuthenticationService, UserService} from '@app/_services';
import {ProjectService} from '@app/_services/project.service';
import {Branche} from '@app/_models/branche';
import {BrancheService} from '@app/_services/branche.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {CommentService} from '@app/_services/comment.service';
import {first} from 'rxjs/operators';
import {Comments} from '@app/_models/Comment';

@Component({
  selector: 'app-get-issues',
  templateUrl: './get-issues.component.html',
  styleUrls: ['./get-issues.component.css']
})
export class GetIssuesComponent implements OnInit {

  currentUser: User;
  currentProject: Project;
  issue: Issues = new Issues();
  creatorIssue : User = new User();
  branche : Branche = new Branche();
  private issueService: IssuesService
  NewCommentForm: FormGroup;
  comments: Comments [];
  link_identifier: string;

  constructor(private formBuilder: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private authenticationService: AuthenticationService,
    private projectService: ProjectService,
    private issuesService: IssuesService,
    private  brancheService : BrancheService,
    private userService: UserService,
              private commentService: CommentService,
              private alertService: AlertService,
  private activatedRoute: ActivatedRoute,
  ) {
    this.issueService = issuesService;

    if (this.authenticationService.currentUserValue){
      this.currentUser  = this.authenticationService.currentUserValue;
      console.log("we have a user " + this.currentUser._id);
    }else{
      console.log("we DONT have a user");
    }


  }
  loading = false;
  ngOnInit() {
   this.loadIssueAndComment();
   this.loadBranche();
   this.loadCreatorIssue();
   this.initForm();
    this.link_identifier = this.activatedRoute.snapshot.params['username'] + '/' + this.activatedRoute.snapshot.params['projectname'];
    console.log('Project to load DATA : ' + this.link_identifier);

    this.projectService.getProjectByIdentifier(this.link_identifier).pipe(first()).subscribe(project => {

      // @ts-ignore
      this.currentProject = project;
    });  }
  initForm(){
    this.NewCommentForm = this.formBuilder.group({
      description: ['', [Validators.required]],
    });

  }

private loadIssueAndComment() {
    this.route.params.subscribe(
      params => {
        this.issuesService.getOneIssues(params['id']).subscribe(
          res => {
            console.log(res);
            this.issue = res;
            this.issue.issued = res['issued']['node'];
          }
        );
        console.log('les branxhes de issues '+  this.issue.issued)
        //this.branche = this.issue.issued[0]
      this.commentService.getCommentforISsues(params['id']).subscribe(
        (res: Comments[]) => {
          this.comments = res ;
        }
      )

      }
    );
}
  private loadBranche() {
console.log('load branche')
    this.brancheService.getBranchebyId(this.issue.branche_id).subscribe(
      res => {
       console.log("branche: "+ res.name );
        this.branche = res;
      }
    );
  }


  private loadCreatorIssue() {

        this.userService.getById(this.issue.creator_id).subscribe(
          res => {
            console.log(res);
            this.creatorIssue = res;
          }
        );


  }
private createComment() {

   // console.log("value form comment"+this.NewCommentForm.value)
  this.commentService.newComment(this.NewCommentForm.value, this.issue._id, this.currentUser._id).pipe(first())
    .subscribe(
      data => {
        this.alertService.success(' successful to create a new comment ', true);
        this.loadIssueAndComment();
        //this.router.navigate(['/project/'+this.currentUser.username+'/'+this.currentProject.name+'/issue/'+this.issue._id]);
        // this.NewCommentForm.value.description = '';
      },
      error => {
        this.alertService.error(error);
        this.loading = false;
      });

}

}
