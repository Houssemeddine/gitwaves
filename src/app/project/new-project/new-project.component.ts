import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ProjectService} from '@app/_services/project.service';
import {Router} from '@angular/router';
import {Project} from '@app/_models/project';
import {first} from 'rxjs/operators';
import {AlertService, AuthenticationService} from '@app/_services';

@Component({
  selector: 'app-new-project',
  templateUrl: './new-project.component.html',
  styleUrls: ['./new-project.component.css']
})
export class NewProjectComponent implements OnInit {
  NewProjectForm: FormGroup;
  @Input() name = '';

  constructor(private formBuilder: FormBuilder,
              private projectService: ProjectService,
              private router: Router,
              private alertService: AlertService,
              private authenticationService: AuthenticationService) {
  }

  loading = false;

  ngOnInit() {
    this.initForm();
  }

  initForm() {
    this.NewProjectForm = this.formBuilder.group({
      name: ['', [Validators.required]],
      description: ['', [Validators.required]],
      id: this.authenticationService.currentUserValue._id,
      creator_id: this.authenticationService.currentUserValue._id,
      username: this.authenticationService.currentUserValue.username,
    });
  }

  onSubmit() {
    this.NewProjectForm.value.link_identifier = this.authenticationService.currentUserValue.username + '/' + this.NewProjectForm.value.name;
    console.log(this.NewProjectForm.value);
    this.projectService.new(this.NewProjectForm.value).pipe(first())
      .subscribe(
        data => {
          this.alertService.success('Project has been created successfully', true);
          this.router.navigate(['/project/' + this.authenticationService.currentUserValue.username + '/' + this.NewProjectForm.value.name]);
        },
        error => {
          this.alertService.error(error);
          this.loading = false;
        });

  }

}
