import { Component, OnInit } from '@angular/core';
import {Branche} from '@app/_models/branche';
import {ActivatedRoute, Router} from '@angular/router';
import {AlertService, AuthenticationService, UserService} from '@app/_services';
import {ProjectService} from '@app/_services/project.service';
import {BrancheService} from '@app/_services/branche.service';
import {User} from '@app/_models';
import {Project} from '@app/_models/project';
import {first} from 'rxjs/operators';
import {Commit} from '@app/_models/Commit';
import {CommitService} from '@app/_services/commit.service';

@Component({
  selector: 'app-graph',
  templateUrl: './graph.component.html',
  styleUrls: ['./graph.component.css']
})
export class GraphComponent implements OnInit {
  branches: Branche[] = [];
  selectedBranche: Branche ;
  currentUser: User;
  currentProject: Project;
  project: Project = new Project();
  link_identifier: string;
  commits: Commit[] = [];
val: number ;
  constructor(  private router: Router,
                private authenticationService: AuthenticationService,
                private projectService: ProjectService,
                private brancheService: BrancheService,
                private activatedRoute: ActivatedRoute,
                private alertService: AlertService,
                private commitService: CommitService,
                private userservice: UserService,
               ) {
    if (this.authenticationService.currentUserValue){
      this.currentUser  = this.authenticationService.currentUserValue;
      console.log("we have a user " + this.currentUser._id);
    }else{
      console.log("we DONT have a user");
    }
    if (this.projectService.currentProjectValue){
      this.currentProject  = this.projectService.currentProjectValue;
      console.log("we have a project " + this.currentProject._id);
    }else{
      console.log("we DONT have a project");
    }
  }

  ngOnInit() {
    this.link_identifier = this.activatedRoute.snapshot.params['username'] + '/' + this.activatedRoute.snapshot.params['projectname'];

    this.projectService.getProjectByIdentifier(this.link_identifier).pipe(first()).subscribe(project => {

      // @ts-ignore
      this.project = Object.assign(new Project(), project.project);
      this.project.branched = Object.assign([], project.branched);
     // this.selectedBranche = this.project.branched[0];
    });
    this.getCommits();
    this.val = 0;
  }
  onSelect(branche: Branche) {
    this.selectedBranche = branche;
    console.log('selected id : ' + branche._id);
    this.getCommits();
    this.val = 0;

  }
  getCommits(){
    this.commitService.getCommits(this.selectedBranche._id).subscribe(res => {

      this.commits = res;
    });
    console.log("aaa"+this.commits)
  }
  getUser(id: string){
    this.userservice.getUserneme(id).subscribe(res => {
      return res;
    })
  }
  getBranche(id: number){
    this.brancheService.getBranchebyId(id).subscribe(res=>{
      return res.name
    });
  }

  getval(){
    this.val = this.val + 30;
    return this.val;
  }
}
