import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {ProjectService} from '@app/_services/project.service';
import {first} from 'rxjs/operators';
import {Project} from '@app/_models/project';
import {AuthenticationService, UserService} from '@app/_services';
import {Branche} from '@app/_models/branche';
import {TreeComponent} from 'gijgo';
import * as types from 'gijgo';
import {BrancheService} from '@app/_services/branche.service';
import {User} from '@app/_models';
import {Subscription} from 'rxjs';
import {Commit} from '@app/_models/Commit';
import {environment} from '@environments/environment';

declare var jQuery: any;

@Component({
  selector: 'app-project',
  templateUrl: './project.component.html',
  styleUrls: ['./project.component.css']
})
export class ProjectComponent implements OnInit {

  link_identifier: string;
  project: Project ;
  brancheList: Branche [] = [];
  selectedBranche: Branche;

  currentUser: User;
  currentUserSubscription: Subscription;

  commit: Commit ;

  @ViewChild('tree') tree: TreeComponent ;

  configuration: types.TreeSettings;

  myConfig: types.TreeSettings;

  eventLog: string = '';
  // TODO : set this to true after load branch
  branchLoaded: boolean = false;
  private receivedTree: any;


  public env = environment ;

  constructor(
    private authenticationService: AuthenticationService,

    private route: ActivatedRoute,
    private projectService: ProjectService,
    private branchService: BrancheService,
    private authentificationService: AuthenticationService,
    private router: Router,
  ) {
    this.currentUserSubscription = this.authenticationService.currentUser.subscribe(user => {
      this.currentUser = user;
    });




    this.configuration = {
      uiLibrary: 'bootstrap4',
      dataSource: [],
      select: (e, node, id) => {
        this.eventLog += '<p>Node with id=' + id + ' is selected.</p>';
      },

      unselect: (e, node, id) => {
        this.eventLog += '<p>Node with id=' + id + ' is unselected.</p>';
      }
    };
  }

  ngOnInit() {
    this.link_identifier = this.route.snapshot.params['username'] + '/' + this.route.snapshot.params['projectname'];
    console.log('Project to load DATA : ' + this.link_identifier);

    this.projectService.getProjectByIdentifier(this.link_identifier).pipe(first()).subscribe(project => {


      /*
      *******************************************************************************
      USE THIS INSTEAD :

      // @ts-ignore
      this.project = Object.assign(new Project(), project.project);
      this.project.branches = Object.assign([], project.branches);
       */
      //console.log('PROJECT DATA RECEIVED: ' + JSON.stringify(project));

      // @ts-ignore
      // console.log(project.project.name);
      //console.log(project["project"]["name"]);
      /*
            // @ts-ignore
            this.project.name = project.project.name;
            // @ts-ignore
            this.project.lastUpdate = project.project.last_update;
            // @ts-ignore
            this.project.stars = project.project.stars;
            // @ts-ignore
            this.project.owner = project.project.owner;
            // @ts-ignore
            this.project.desciption = project.project.desciption;
            // @ts-ignore
            this.project.etat = project.project.etat;
            // @ts-ignore
            this.project.date = project.project.date;
            // @ts-ignore
            this.project.id = project.project['_id'];
            // @ts-ignore
            this.project.forked = project.project['forked'];
      */

      this.project = Object.assign(new Project(), project);

      console.log('received this ' + JSON.stringify(project.creator_id.username));
      this.project = project;
      this.brancheList = project.branched;

      this.projectService.currentProjectValue = project;
// @ts-ignore
      // this.project = Object.assign(new Project(), project);
      //this.project.branches = Object.assign([], project.branches);

      /*
            var ar: any;
            // @ts-ignore
            for (ar in project.branches) {
              // @ts-ignore
              // console.log(project.branches[ar]['name']);
              //console.log(project.branches[ar]);
              let branche = new Branche();
              // @ts-ignore
              branche.id = project.branches[ar]['_id'];
              // @ts-ignore
              branche.name = project.branches[ar]['name'];
              // @ts-ignore
              branche.last_update = project.branches[ar]['last_update'];
              // @ts-ignore
              branche.hash_value = project.branches[ar]['hash_value'];
              // @ts-ignore
              branche.created_at = project.branches[ar]['created_at'];

              this.brancheList.push(branche);

              //console.log(ar);
              if (ar == 0) {
                this.selectedBranche = branche;
              }
            }
      */
    });

  }

  expandAll() {
    this.tree.instance.expandAll();
  }

  collapseAll() {
    this.tree.instance. collapseAll();
  }

  forkProject(id: number) {
    // need to check for
    console.log('FORKING THIS PROJECT with ID : ' + id);
    this.projectService.forkProjectWithUserIDAndProjectID(
      parseInt(this.authentificationService.currentUserValue._id),
      this.project.id,
      this.project
    ).subscribe(res => {
      console.log('Received dis : ' + res);
      this.router.navigate(['/']);

    });
  }

  onSelect(branche: Branche): void {
    this.selectedBranche = branche;
    console.log(this.project);
    console.log('Sendint THIS : ' + this.project.link_identifier + 'name : ' + branche.name);

    this.branchService.loadBrancheContent(this.link_identifier, branche.name)
      .pipe(first())
      .subscribe(content => {
        console.log('received this : ' + JSON.stringify(content));
        if (JSON.stringify(content) === "{}"){
          this.receivedTree = [];
          return;
        }

        this.branchLoaded = true;
        // @ts-ignore
        this.commit = Object.assign(new Commit(), content.last_commit);

        //console.log(this.commit);


        this.receivedTree = content["tree"];
        console.log (this.receivedTree);

      });
  }

  starProject() {
    console.log('staring this ');
    this.projectService.startProject(this.project._id);
  }
}
