import {AfterViewInit, Component, Input, OnInit, ViewChild} from '@angular/core';
import {ProjectService} from '@app/_services/project.service';
import {ActivatedRoute, Router} from '@angular/router';
import {AlertService, AuthenticationService} from '@app/_services';
import {ProjectComponent} from '@app/project/project.component';
import {Branche} from '@app/_models/branche';
import {BrancheService} from '@app/_services/branche.service';
import {catchError, first} from 'rxjs/operators';
import {Project} from '@app/_models/project';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-new-branch',
  templateUrl: './new-branch.component.html',
  styleUrls: ['./new-branch.component.css']
})
export class NewBranchComponent implements OnInit {

  @Input() branchName: string;

  branches: Branche[] = [];
  selectedBranche: Branche;

  link_identifier: string;
  project: Project = new Project();
  brancheList: Branche [] = [];
  currentProject: Project;
  NewBrancheForm: FormGroup;

  constructor(
    private brancheService: BrancheService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private alertService: AlertService,
    private authenticationService: AuthenticationService,
    private projectService: ProjectService,
    private formBuilder: FormBuilder
  ) {

  }

  ngOnInit() {
    this.link_identifier = this.activatedRoute.snapshot.params['username'] + '/' + this.activatedRoute.snapshot.params['projectname'];
  this.projectService.getProjectByIdentifier(this.link_identifier).subscribe(
    res =>{
      this.currentProject  = res;
    }
  );

    console.log('Project to load DATA : ' + this.link_identifier);
    this.NewBrancheForm = this.formBuilder.group({
      branchName: ['', [Validators.required , Validators.minLength(3),
      ]],
    });
    this.projectService.getProjectByIdentifier(this.link_identifier).pipe(first()).subscribe(project => {

      // @ts-ignore
      this.project = Object.assign(new Project(), project.project);
      this.project.branched = Object.assign([], project.branched);
      this.selectedBranche = this.project.branched[0];
    });
  }

  createBranche() {
    console.log('Branche  ' + this.branchName);
    if (this.branchName.length < 2) {
      this.alertService.error('Please insert a valid branch name');
    } else {
      let tmp: Branche = new Branche();
      tmp.name = this.branchName;
      this.brancheService.newBranche(
        tmp,
        this.currentProject._id,
        this.selectedBranche._id
      ).subscribe(
        data => {
          console.log('redirecting');
          this.alertService.success('Created Branche successfully', true);
          this.router.navigate(['/project/' + this.link_identifier + '/branches']);
        },
        error => {
          this.alertService.error(error);
        });
    }
  }

  onSelect(branche: Branche) {
    this.selectedBranche = branche;
    console.log('selected id : ' + branche._id);
  }
}
