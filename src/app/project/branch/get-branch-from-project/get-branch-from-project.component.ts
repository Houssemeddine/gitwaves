import {Component, OnInit} from '@angular/core';
import {Branche} from '@app/_models/branche';
import {Project} from '@app/_models/project';
import {BrancheService} from '@app/_services/branche.service';
import {ActivatedRoute, Router} from '@angular/router';
import {AlertService, AuthenticationService} from '@app/_services';
import {ProjectService} from '@app/_services/project.service';
import {first} from 'rxjs/operators';
import {User} from '@app/_models';
import {environment} from '@environments/environment';

@Component({
  selector: 'app-get-branch-from-project',
  templateUrl: './get-branch-from-project.component.html',
  styleUrls: ['./get-branch-from-project.component.css']
})
export class GetBranchFromProjectComponent implements OnInit {
  brancheList: Branche [] = [];
  currentProject: Project;
  link_identifier: string;
  project: Project = new Project();
  selectedBranche: Branche;
  currentUser: User = new User();
  private projectName: string;
  private userProject: string;

  public env = environment ;



  constructor(
    private brancheService: BrancheService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private alertService: AlertService,
    private authenticationService: AuthenticationService,
    private projectService: ProjectService
  ) {
    if (this.projectService.currentProjectValue) {
      this.currentProject = this.projectService.currentProjectValue;
      this.authenticationService.currentUser.subscribe(x => this.currentUser = x);
      console.log('we have a user ' + this.currentProject._id);
    } else {
      console.log('we DONT have a user');
    }
  }

  ngOnInit() {
    this.link_identifier = this.activatedRoute.snapshot.params['username'] + '/' + this.activatedRoute.snapshot.params['projectname'];
    console.log('Project to load DATA : ' + this.link_identifier);

    this.projectName = this.activatedRoute.snapshot.params['projectname'];
    this.userProject = this.activatedRoute.snapshot.params['username'];

    this.projectService.getProjectByIdentifier(this.link_identifier).pipe(first()).subscribe(project => {

      // @ts-ignore
      this.project = Object.assign(new Project(), project.project);
      this.project.branched = Object.assign([], project.branched);
      this.selectedBranche = this.project.branched[0];
    });
  }

  brancheMaster(nom: string) {
    if (nom == 'master') {
      return true;
    }
    return false;

  }
}
