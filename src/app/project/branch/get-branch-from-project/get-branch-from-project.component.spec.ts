import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GetBranchFromProjectComponent } from './get-branch-from-project.component';

describe('GetBranchFromProjectComponent', () => {
  let component: GetBranchFromProjectComponent;
  let fixture: ComponentFixture<GetBranchFromProjectComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GetBranchFromProjectComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GetBranchFromProjectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
