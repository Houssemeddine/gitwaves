import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BrancheDataComponent } from './branche-data.component';

describe('BrancheDataComponent', () => {
  let component: BrancheDataComponent;
  let fixture: ComponentFixture<BrancheDataComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BrancheDataComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BrancheDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
