import {Component, OnInit} from '@angular/core';
import {Project} from '@app/_models/project';
import {Branche} from '@app/_models/branche';
import {User} from '@app/_models';
import {Subscription} from 'rxjs';
import {Commit} from '@app/_models/Commit';
import {AuthenticationService} from '@app/_services';
import {ActivatedRoute, Router} from '@angular/router';
import {ProjectService} from '@app/_services/project.service';
import {BrancheService} from '@app/_services/branche.service';
import {first} from 'rxjs/operators';

import {environment} from '@environments/environment';

@Component({
  selector: 'app-branche-data',
  templateUrl: './branche-data.component.html',
  styleUrls: ['./branche-data.component.css']
})
export class BrancheDataComponent implements OnInit {

  public env = environment ;

  link_identifier: string;
  project: Project;
  brancheList: Branche [] = [];
  selectedBranche: Branche;

  currentUser: User;
  currentUserSubscription: Subscription;

  selectedCommit: Commit;

  eventLog: string = '';
  // TODO : set this to true after load branch
  branchLoaded: boolean = false;
  private receivedTree: any;

  commits: [Commit];
  brancheName: String;

  constructor(
    private authenticationService: AuthenticationService,
    private route: ActivatedRoute,
    private projectService: ProjectService,
    private branchService: BrancheService,
    private authentificationService: AuthenticationService,
    private router: Router) {
    this.currentUserSubscription = this.authenticationService.currentUser.subscribe(user => {
      this.currentUser = user;
    });
  }

  ngOnInit(): void {

    this.link_identifier = this.route.snapshot.params['username'] + '/' + this.route.snapshot.params['projectname'];
    console.log('Project to load DATA : ' + this.link_identifier);

    this.projectService.getProjectByIdentifier(this.link_identifier).pipe(first()).subscribe(project => {

      console.log("Branche to load commits : ",this.route.snapshot.params['branchename']);

      this.project = Object.assign(new Project(), project);

     // console.log('received this ' + JSON.stringify(project));
      this.project = project;
      this.brancheList = project.branched;

      this.projectService.currentProjectValue = project;

      this.brancheName = this.route.snapshot.params['branchename'];

      let branche : Branche = null;

      for (let br in project.branched){
        if (project.branched[br].name === this.route.snapshot.params['branchename']){
          branche = project.branched[br];
          //this.commits=branche["commited"];

          this.commits = Object.assign([Commit], branche["commited"]);

          console.log('received this ' + JSON.stringify( this.commits));
          //this.commits = JSON.stringify( this.commits);

          break;
        }
      }
      if (branche == null){
        this.router.navigateByUrl("/");
      }

// @ts-ignore
      // this.project = Object.assign(new Project(), project);
      //this.project.branches = Object.assign([], project.branches);

      /*
            var ar: any;
            // @ts-ignore
            for (ar in project.branches) {
              // @ts-ignore
              // console.log(project.branches[ar]['name']);
              //console.log(project.branches[ar]);
              let branche = new Branche();
              // @ts-ignore
              branche.id = project.branches[ar]['_id'];
              // @ts-ignore
              branche.name = project.branches[ar]['name'];
              // @ts-ignore
              branche.last_update = project.branches[ar]['last_update'];
              // @ts-ignore
              branche.hash_value = project.branches[ar]['hash_value'];
              // @ts-ignore
              branche.created_at = project.branches[ar]['created_at'];

              this.brancheList.push(branche);

              //console.log(ar);
              if (ar == 0) {
                this.selectedBranche = branche;
              }
            }
      */
    });
  }


  onSelect(commit: Commit): void {
    this.selectedCommit = commit;

    console.log('Sendint THIS : ' + this.project.link_identifier + ' name : ' + this.selectedBranche + "  " +this.selectedCommit);

    this.branchService.loadCommitContent(this.link_identifier, this.brancheName, this.selectedCommit.timestamp)
      .pipe(first())
      .subscribe(content => {
        console.log('received this : ' + JSON.stringify(content));
        if (JSON.stringify(content) === "{}")
          return;

        this.branchLoaded = true;
        // @ts-ignore
        this.commit = Object.assign(new Commit(), content.last_commit);

        //console.log(this.commit);


        this.receivedTree = content["tree"];
        console.log (this.receivedTree);

      });
  }


}
