import {Component, OnInit} from '@angular/core';
import {first} from 'rxjs/operators';
import {Project} from '@app/_models/project';
import {User} from '@app/_models';
import {Subscription} from 'rxjs';
import {ProjectService} from '@app/_services/project.service';
import {AuthenticationService, UserService} from '@app/_services';
import {Paginatour} from '@app/_models/Paginatour';

@Component({
  selector: 'app-projects',
  templateUrl: './projects.component.html',
  styleUrls: ['./projects.component.css']
})
export class ProjectsComponent implements OnInit {
  currentUser: User;
  currentUserSubscription: Subscription;
  users: User[] = [];

  projectService: ProjectService;
  subscribedToProjects: Project[] = [];
  ownedProject: Project[] = [];

  loadedProjects: Project[] = [];

  constructor(
    private authenticationService: AuthenticationService,
    private userService: UserService,
    private  prjService: ProjectService
  ) {
    this.currentUserSubscription = this.authenticationService.currentUser.subscribe(user => {
      this.currentUser = user;
      this.projectService = prjService;
    });
  }

  ngOnInit() {

    console.log('Loading all projects : ');
    var paginator: Paginatour = new Paginatour();

    paginator.limit = 10;
    paginator.order = '';
    paginator.skip = 0;

    this.projectService.getAllProject(paginator)
      .pipe(first()).subscribe(projects => {
      console.log(projects);
      this.loadedProjects = projects;
    });
  }

}
