import { Component, OnInit } from '@angular/core';
import {User} from '@app/_models';
import {Router} from '@angular/router';
import {AuthenticationService} from '@app/_services';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {

  currentUser: User;

  constructor(
    private router: Router,
    private authenticationService: AuthenticationService
  ) {
    if (this.authenticationService.currentUserValue){
      this.currentUser  = this.authenticationService.currentUserValue;
      console.log("we have a user " + this.currentUser._id);
    }else{
      console.log("we DONT have a user");
    }
  }

  ngOnInit() {
    // check for connected user !! if not connected ==> show login /register

  }
  logout() {
    this.authenticationService.logout();
    this.router.navigate(['/login']);
  }

}
