import { Component, OnInit } from '@angular/core';
import {User} from '../../_models';
import {ActivatedRoute, Router} from '@angular/router';
import {AuthenticationService} from '../../_services';
import {Project} from '@app/_models/project';
import {ProjectService} from '@app/_services/project.service';
import {first} from 'rxjs/operators';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  link_identifier: string;

  currentUser: User;
  currentProject: Project;

  constructor(
    private router: Router,
    private authenticationService: AuthenticationService,
    private projectService: ProjectService,
    private activatedRoute: ActivatedRoute,

  ) {
    if (this.authenticationService.currentUserValue) {
      this.currentUser  = this.authenticationService.currentUserValue;
      console.log("we have a user " + this.currentUser._id);
    }else{
      console.log("we DONT have a user");
    }


  }

  ngOnInit() {
    // check for connected user !! if not connected ==> show login /register
    this.link_identifier = this.activatedRoute.snapshot.params['username'] + '/' + this.activatedRoute.snapshot.params['projectname'];
    console.log('Project to load DATA : ' + this.link_identifier);

    this.projectService.getProjectByIdentifier(this.link_identifier).pipe(first()).subscribe(project => {

      // @ts-ignore
      this.currentProject = project;
    });
    console.log("aaaaaaé"+this.currentProject)

    console.log("aaaaaaaaaaa : "+this.projectService.currentProjectValue.name);
    if (this.projectService.currentProjectValue){
      this.currentProject  = this.projectService.currentProjectValue;
      console.log("we have a project " + this.currentProject._id);
    }else{
      console.log("we DONT have a project");
    }
  }

  logout() {
    this.authenticationService.logout();
    this.router.navigate(['/login']);
  }
}
