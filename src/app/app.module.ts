﻿import {GraphComponent} from './project/graph/graph.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
﻿﻿﻿import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule, HTTP_INTERCEPTORS} from '@angular/common/http';
import {NgbPaginationModule, NgbAlertModule} from '@ng-bootstrap/ng-bootstrap';
// used to create fake backend
import {fakeBackendProvider} from './_helpers';

import {AppComponent} from './app.component';
import {routing} from './app.routing';

import {AlertComponent} from './_components';
import {JwtInterceptor, ErrorInterceptor} from './_helpers';
import {HomeComponent} from './home';
import {LoginComponent} from './login';
import {RegisterComponent} from './register';
import {ProjectComponent} from './project/project.component';
import {ProfileComponent} from './profile/profile.component';
import {MembresComponent} from './membres/membres.component';
import {NavbarComponent} from './template/navbar/navbar.component';
import {NewProjectComponent} from './project/new-project/new-project.component';
import {UploadModule} from './upload/upload.module';

import {TimeAgoPipe} from 'time-ago-pipe';
import {BranchComponent} from './project/branch/branch.component';
import {RouterModule} from '@angular/router';
import { NewBranchComponent } from './project/branch/new-branch/new-branch.component';
import {TreeComponent} from '@app/_components/tree.component';
import { ProjectsComponent } from './project/projects/projects.component'
;
import { GetBranchFromProjectComponent } from './project/branch/get-branch-from-project/get-branch-from-project.component'
;
import { IssuesComponent } from './project/issues/issues.component';
import { NewIssuesComponent } from './project/issues/new-issues/new-issues.component'
;
import { MenuComponent } from './template/menu/menu.component';
import { GetIssuesComponent } from './project/issues/get-issues/get-issues.component';;
import { HelpComponent } from './help/help.component';
import { BrancheDataComponent } from './project/branch/branche-data/branche-data.component'
import {NgxAudioPlayerModule} from 'ngx-audio-player';


@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    UploadModule,
    routing,
    NgbModule,
    NgbPaginationModule,
    NgbAlertModule,
    NgxAudioPlayerModule,
  ],
  declarations: [
    AppComponent,
    AlertComponent,
    HomeComponent,
    LoginComponent,
    RegisterComponent,
    ProjectComponent,
    ProfileComponent,
    MembresComponent,
    GraphComponent,
    NavbarComponent,
    NewProjectComponent,
    TimeAgoPipe,
    BranchComponent,
    NewBranchComponent,
    TreeComponent,
    ProjectsComponent ,
    GetBranchFromProjectComponent,
    IssuesComponent
,
    NewIssuesComponent
,
    MenuComponent,
    GetIssuesComponent ,
    HelpComponent ,
    BrancheDataComponent   ],
  providers: [
    {provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true},
    {provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true},

    // provider used to create fake backend
    fakeBackendProvider
  ],
  bootstrap: [AppComponent],
})

export class AppModule {
}
